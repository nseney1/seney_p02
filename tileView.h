//
//  tileView.h
//  Seney_P02
//
//  Created by Nicholas Ryan Seney on 2/8/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tileView : UIView
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;
@property (nonatomic, strong) IBOutlet UILabel *sublabel;
@property (nonatomic, strong) IBOutlet UIView *colorview;

-(void)inittxt;
-(NSString*)getLabel;
-(void)setLabeltxt:(NSString*)stringtxt;
-(void)changeColorYells;
-(void)changeColorSalmon;

@end
