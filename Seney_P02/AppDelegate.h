//
//  AppDelegate.h
//  Seney_P02
//
//  Created by Nicholas Ryan Seney on 2/7/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

