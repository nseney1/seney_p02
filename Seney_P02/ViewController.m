//
//  ViewController.m
//  Seney_P02
//
//  Created by Nicholas Ryan Seney on 2/7/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "ViewController.h"
#import "tileView.h"

@interface ViewController ()

@end

@implementation ViewController
int SpawnCounter = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self firstSetup];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)firstSetup
{
    int count = 1;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            [tview inittxt];
            NSLog(@"%d",count);
            count++;
        }
    }
}

-(IBAction)rightButtonClick:(id)rightButton
{
    NSLog(@"I pushed right button");
    int needShift[16];
    tileView* temp[16];
    for(int k=0;k<16;k++)
        needShift[k] = 0;
    int i = 0;
    int j = 0;
    while(j<4)
    {
        i=0;
        for(int k=0;k<16;k++)
            temp[k] = nil;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                if(![[tview getLabel] isEqualToString:(@"")])
                    needShift[i] = 1;
                else
                    needShift[i] = 0;
                i++;
            }
            if(i>15)
                break;
        }
        i = 0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                temp[i] = tview;
                i++;
            }
        }
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                if(needShift[i] == 1 && needShift[i+4] == 0 && i<12)
                {
                    [temp[i+4] setLabeltxt:[tview getLabel]];
                    [temp[i+4] changeColorSalmon];
                    [tview changeColorYells];
                    needShift[i] = 0;
                }
                i++;
            }
            
        }
        j++;
    }
    [self checkMyGrid:1];
    i = 0;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            temp[i] = tview;
            i++;
        }
    }
    SpawnCounter++;
    if(SpawnCounter%4 == 0)
    {
        int random = arc4random();
        bool forceLoop = true;
        while(forceLoop)
        {
            if(random<0)
                random = random * -1;
            if([[temp[random%16]getLabel] isEqual:@""])
            {
                [temp[random%16] setLabeltxt:@"2"];
                forceLoop = false;
            }
            random = arc4random();
            
        }
    }
    
}


-(IBAction)leftButtonClick:(id)leftButton
{
    NSLog(@"I pushed left button");
    int needShift[16];
    tileView* temp[16];
    for(int k=0;k<16;k++)
        needShift[k] = 0;
    int i = 0;
    int j = 0;
    while(j<4)
    {
        for(int k=0;k<16;k++)
            temp[k] = nil;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                if([[tview getLabel] isEqualToString:(@"")])
                    needShift[i+4] = 1;
                else
                    needShift[i+4] = 0;
                i++;
            }
            if(i>11)
                break;
        }
        i = 0;
        
    
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                if(i<12 && needShift[i+4] == 1)
                {
                    temp[i] = tview;
                }
                if(needShift[i] == 1 && !([[tview getLabel] isEqual:(@"")]))
                {
                    [temp[i-4] setLabeltxt:[tview getLabel]];
                    [temp[i-4] changeColorSalmon];
                    [tview changeColorYells];
                    needShift[i] = 0;
                /*if(i<11)
                    needShift[i+4] = 1;*/
                }
                i++;
            }
        }
        j++;
        i=0;
    }
    [self checkMyGrid:0];
    i = 0;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            temp[i] = tview;
            i++;
        }
    }
    SpawnCounter++;
    if(SpawnCounter%4 == 0)
    {
        int random = arc4random();
        bool forceLoop = true;
        while(forceLoop)
        {
            if(random<0)
                random = random * -1;
            if([[temp[random%16]getLabel] isEqual:@""])
            {
                [temp[random%16] setLabeltxt:@"2"];
                [temp[random%16] changeColorSalmon];
                forceLoop = false;
            }
            random = arc4random();
            
        }
    }
}

-(IBAction)upButtonClick:(id)upButton
{
    NSLog(@"I pushed up button");
    int needShift[16];
    tileView* temp[16];
    for(int k=0;k<16;k++)
        needShift[k] = 0;
    int i = 0;
    int j = 0;
    while(j<4)
    {
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                temp[i] = tview;
                i++;
            }
        }
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                NSLog(@"%d",i);
                
                if([[tview getLabel] isEqualToString:(@"")] && ![[temp[i+1]getLabel]isEqual:@""])
                    needShift[i+1] = 1;
                else
                    needShift[i+1] = 0;
                i++;
                if(i>14)
                    break;
            }
            if(i>14)
                break;
        }
        
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                if(i>0 && i%4==3)
                    ;
               else if(i<15 && needShift[i+1] == 1)
                {
                    [tview setLabeltxt:[temp[i+1] getLabel]];
                    [tview changeColorSalmon];
                    [temp[i+1]changeColorYells];
                    needShift[i+1] = 0;
                }
                i++;
                if(i==15)
                    break;
            }
        }
        j++;
    }
    [self checkMyGrid:2];
    i = 0;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            temp[i] = tview;
            i++;
        }
    }
    SpawnCounter++;
    if(SpawnCounter%4 == 0)
    {
        int random = arc4random();
        bool forceLoop = true;
        while(forceLoop)
        {
            if(random<0)
                random = random * -1;
            if([[temp[random%16]getLabel] isEqual:@""])
            {
                [temp[random%16] setLabeltxt:@"2"];
                [temp[random%16] changeColorSalmon];
                forceLoop = false;
            }
            random = arc4random();
            
        }
    }
}

-(IBAction)downButtonClick:(id)downButton
{
    NSLog(@"I pushed up button");
    int needShift[16];
    tileView* temp[16];
    for(int k=0;k<16;k++)
        needShift[k] = 0;
    int i = 0;
    int j = 0;
    while(j<4)
    {
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                temp[i] = tview;
                i++;
            }
        }
        i=0;
            for(UIStackView* view in _superStackView.arrangedSubviews)
            {
                for(tileView* tview in view.arrangedSubviews)
                {
                    if(![[tview getLabel] isEqualToString:(@"")])
                        needShift[i] = 1;
                    else
                        needShift[i] = 0;
                    i++;
                }
                if(i>15)
                    break;
            }
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                if(i>0 && i%4==3)
                    ;
                else if(needShift[i] == 1 && needShift[i+1] == 0)
                {
                    [temp[i+1] setLabeltxt:[tview getLabel]];
                    [temp[i+1] changeColorSalmon];
                    [tview changeColorYells];
                    needShift[i+1] = 0;
                }
                i++;
                
            }
            
        }
        j++;
    }
    [self checkMyGrid:3];
    i = 0;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            temp[i] = tview;
            i++;
        }
    }
    SpawnCounter++;
    if(SpawnCounter%4 == 0)
    {
        int random = arc4random();
        bool forceLoop = true;
        while(forceLoop)
        {
            if(random<0)
                random = random * -1;
            if([[temp[random%16]getLabel] isEqual:@""])
            {
                [temp[random%16] setLabeltxt:@"2"];
                [temp[random%16] changeColorSalmon];
                forceLoop = false;
            }
            random = arc4random();
            
        }
    }
}

-(void)checkMyGrid:(int)inttest
{
    //0 = left, 1 = right, 2 = up, 3 = down
    int i = 0;
    tileView* temp[16];
    for(int o=0;o<16;o++)
        temp[o] = nil;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            temp[i] = tview;
            i++;
        }
    }
    i=0;
    if(inttest == 0)
    {
       
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                if([[tview getLabel] isEqual:([temp[i+4] getLabel])] && ![([tview getLabel])
                                                                            isEqual:@""])
                {
                    [self combonation:tview:temp[i+4]];
                }
                i++;
            }
            if(i>11)
                break;
          
        }
    }
    else if(inttest == 1)
    {
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                
                if([[tview getLabel] isEqual:([temp[i+4] getLabel])] && ![([tview getLabel])
                                                                          isEqual:@""])
                {
                    
                    [self combonation:temp[i+4]:tview];
                }
                i++;
            }
            if(i>11)
                break;
            
        }
    }
    else if(inttest == 2)
    {
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                
                if([[tview getLabel] isEqual:([temp[i+1] getLabel])] && ![([tview getLabel])
                                                                          isEqual:@""])
                {
                    
                    [self combonation:tview:temp[i+1]];
                }
                i++;
                
            }
            if(i>14)
                break;
            
            
        }
    }
    else if(inttest == 3)
    {
        i=0;
        for(UIStackView* view in _superStackView.arrangedSubviews)
        {
            for(tileView* tview in view.arrangedSubviews)
            {
                
                if([[tview getLabel] isEqual:([temp[i+1] getLabel])] && ![([tview getLabel])
                                                                          isEqual:@""])
                {
                    
                    [self combonation:temp[i+1]:tview];
                }
                i++;
                
            }
            if(i>14)
                break;
            
            
        }
    }
    
}

-(void)combonation: (tileView*)tile1: (tileView*)tile2
{
    int newNum = [tile1 getLabel].intValue * 2;
    NSString *newLabel = [NSString stringWithFormat:@"%d",newNum];
    [tile1 setLabeltxt: newLabel];
    [tile2 changeColorYells];
    
    
    /*tileView* temp[16];
    int i=0;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            temp[i] = tview;
            i++;
        }
    }
    int random = arc4random();
    bool forceLoop = true;
    while(forceLoop)
    {
        if(random<0)
            random = random * -1;
        if([[temp[random%16]getLabel] isEqual:@""])
        {
            [temp[random%16] setLabeltxt:@"2"];
            [temp[random%16] changeColorSalmon];
            forceLoop = false;
        }
        random = arc4random();
        
    }*/

}

@end
