//
//  ViewController.h
//  Seney_P02
//
//  Created by Nicholas Ryan Seney on 2/7/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic,retain) IBOutlet UIStackView *superStackView;
@property (nonatomic,retain) IBOutlet UIStackView *subStackView0;
@property (nonatomic,retain) IBOutlet UIStackView *subStackView1;
@property (nonatomic,retain) IBOutlet UIStackView *subStackView2;
@property (nonatomic,retain) IBOutlet UIStackView *subStackView3;
@property (nonatomic,retain) IBOutlet UIButton *rightButton;
@property (nonatomic,retain) IBOutlet UIButton *downButton;
@property (nonatomic,retain) IBOutlet UIButton *leftButton;
@property (nonatomic,retain) IBOutlet UIButton *upButton;



-(void)checkMyGrid:(int)inttest;
-(IBAction)rightButtonClick:(id)rightButton;
-(IBAction)leftButtonClick:(id)leftButton;
-(IBAction)upButtonClick:(id)upButton;
-(IBAction)downButtonClick:(id)downButton;



@end

