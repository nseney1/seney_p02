//
//  tileView.m
//  Seney_P02
//
//  Created by Nicholas Ryan Seney on 2/8/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "tileView.h"

@implementation tileView
@synthesize sublabel;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self commonInit];
    }
    return self;
}

-(void) commonInit
{
    _customConstraints = [[NSMutableArray alloc] init];
    UIView *view = nil;
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"tileView"
                                                     owner:self
                                                   options:nil];
    for (id object in objects) {
        if ([object isKindOfClass:[UIView class]]) {
            view = object;
            break;
        }
    }
    
    if (view != nil) {
        _containerView = view;
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:view];
        [self setNeedsUpdateConstraints];
    }
}

- (void)updateConstraints
{
    [self removeConstraints:self.customConstraints];
    [self.customConstraints removeAllObjects];
    
    if (self.containerView != nil) {
        UIView *view = self.containerView;
        NSDictionary *views = NSDictionaryOfVariableBindings(view);
        
        [self.customConstraints addObjectsFromArray:
         [NSLayoutConstraint constraintsWithVisualFormat:
          @"H:|[view]|" options:0 metrics:nil views:views]];
        [self.customConstraints addObjectsFromArray:
         [NSLayoutConstraint constraintsWithVisualFormat:
          @"V:|[view]|" options:0 metrics:nil views:views]];
        
        [self addConstraints:self.customConstraints];
    }
    
    [super updateConstraints];
}


-(void)inittxt
{
    self.sublabel = sublabel;
    int random = arc4random();
    if(random%2 == 0)
    {
        sublabel.text = @"2";
        
    }
    else
    {
        sublabel.text = @"";
        _colorview.backgroundColor = [UIColor yellowColor];
        
    }
}

-(NSString*)getLabel
{
    return sublabel.text;
}

-(void)setLabeltxt:(NSString*)stringtxt
{
    sublabel.text = stringtxt;
    
}

-(void)changeColorSalmon
{
    _colorview.backgroundColor = [UIColor colorWithRed:0.98 green:0.50 blue:0.45 alpha:1.0];
}
-(void)changeColorYells
{
    sublabel.text = @"";
    _colorview.backgroundColor = [UIColor yellowColor];
}

@end
